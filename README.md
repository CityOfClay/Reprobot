Having some fun with Blender: "Clanking Replicator Digestive Tract". 

Pros:
1. Molten child material never touches the parent, so the parent can make children out of materials more robust than its own.
2. No moving parts, so can be printed as a single block.
3. Ion beam deposition is very precise, enough to print circuits and capacitors.
4. While regolith dust can be sifted and poured into the top, even if no one feeds it, floating charged dust particles on the Moon will gradually settle on to the top plate and be consumed. It is always eating and building as long as it has power.
5. Every part of this is something that has been done before.

Cons:
1. This assumes a significant amount of regolith is electrically conductive. There is some evidence for this but we need more samples. If it is not conductive, use electrostatic levitation instead?
2. Ion beam deposition is slow, because it is so fine. Maybe a less precise, higher-volume version is possible?
3. Probably an energy hog. Simplicity is prioritized over efficiency. Probably 99% of this thing's life would be spent printing solar cells.
4. It doesn't poop. Any realistic refinery is going to have waste products, so you'd need an extra ion chute to project the waste away from child production.
5. None of these processes have been tested together.


Links showing existing development of some of the parts:

"Lunar dustbuster" pushes regolith dust across surfaces by peristaltic oscillation of static charges:
science.nasa.gov/science-news/science-at-nasa/2006/19apr_dustbuster
www.spaceflightinsider.com/missions/hawaiian-students-test-lunar-dust-buster-nasa-ames/

Using microwaves to convert glass to plasma:
www.youtube.com/watch?v=cskB5c0mJ58
Explanation of this thermal runaway effect:
www.sciencealert.com/did-this-piece-of-glass-really-break-a-law-of-thermodynamics

Levitated induction melting (Just google "induction levitation melting" or "cold crucible"):
www.youtube.com/watch?v=8i2OVqWo9s0 

CisLunar Industries is now using this approach in real life! 
https://www.cislunarindustries.com/copy-of-news-media

"Levitation Zone Refining and Distillation of Plutonium Metal" (Using induction levitation melting as a smelter, to purify metal)
fas.org/sgp/othergov/doe/lanl/lib-www/la-pubs/00418618.pdf

Lunar regolith can be surprisingly conductive, especially when hot:
articles.adsabs.harvard.edu//full/1971Moon....2..408S/0000408.000.html

Electrospray ionization
en.wikipedia.org/wiki/Electrospray_ionization
"Liquid metal ion source"
en.wikipedia.org/wiki/Liquid_metal_ion_source

